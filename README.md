JQZ is a successful Sydney based property development company with our own in-house building business established solely to undertake our construction works. This integration provides us with strict quality control over all projects. Since our inception we have grown substantially while still diligently maintaining the important family values instilled by our founder and Managing Director Jian Qiu Zhang.

With a strong focus on safety, quality and ethics, JQZ embraces changes and advancements within the industry and thrives on innovation, continuous improvement and best building practices. This allows us to deliver superior properties which any prospective purchaser would be proud to own.

Website : http://www.jqz.com.au/